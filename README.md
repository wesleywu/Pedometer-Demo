# iOS计步器 Demo
iOS 8 (或以上) 使用 Pedometor-iOS-Library 展示每日及历史步数、距离的演示项目。

主界面实现了两个主要功能：

* 查询历史数据：可以设定另外的起始日期和结束日期
* 查询今天数据

项目需要安装 cocoapods，如果未安装，请执行如下命令行先安装和初始化 pods

```bash
sudo gem updte --system
gem source --remove https://rubygems.org/
gem source -a https://ruby.taobao.org/
sudo gem install cocoapods
pod setup
```

项目 clone 下来后，在打开项目编译之前，请在项目目录中先获取依赖的 pod

```bash
git clone https://gitlab.com/wesleywu/Pedometer-Demo.git
cd Pedometer-Demo
pod install
```

请特别注意，对于 iOS 10，应该在 Info.plist 文件中增加（本 demo 项目已添加）

```
	<key>NSHealthShareUsageDescription</key>
	<string>请授权我们访问您每天走过的步数</string>
	<key>NSMotionUsageDescription</key>
	<string>请授权我们访问您每天走过的步数</string>
```

否则会在运行期抛出异常。