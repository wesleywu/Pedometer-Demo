//
//  HistoricViewController.m
//  Pedometer
//
//  Created by Jay Versluis on 31/10/2015.
//  Copyright © 2015 Pinkstone Pictures LLC. All rights reserved.
//

#import "HistoricViewController.h"
#import "DatePickerViewController.h"
#import <Pedometer-iOS-Library/PedometerLib.h>

@interface HistoricViewController ()
@property (strong, nonatomic) IBOutlet UILabel *startDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *endDateLabel;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (strong, nonatomic) PedometerLib *pedometerLib;

@end

@implementation HistoricViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self startDate];
    [self endDate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (PedometerLib *)pedometerLib {
    if ((!_pedometerLib)) {
        _pedometerLib = [PedometerLib sharedPedometerLib];
    }
    return _pedometerLib;
}

# pragma mark - Buttons

- (NSDate *)startDate {
    
    // set the start date to yesterday
    if (!_startDate) {
        NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [[NSDateComponents alloc]init];
        [components setDay:-1];
        NSDate *today = [NSDate date];
        NSDate *yesterday = [calendar dateByAddingComponents:components toDate:today options:0];
        _startDate = yesterday;
        
        // reflect this fact on our label too
        self.startDateLabel.text = [self turnDateIntoString:_startDate];
    }
    return _startDate;
}

- (NSDate *)endDate {
    
    if (!_endDate) {
        
        // make sure the end date is today
        _endDate = [NSDate date];
        
        // reflect this fact on our label too
        self.endDateLabel.text = [self turnDateIntoString:_endDate];
    }
    return _endDate;
}

- (IBAction)startDateEntered:(id)sender {
    
    [self showDatePickerWithType:@"startDate"];
    
}

- (IBAction)endDateEnterted:(id)sender {
    
    [self showDatePickerWithType:@"endDate"];
}

- (IBAction)allStepsToday:(id)sender {
//    [self presentPedometerData:[self.pedometerLib getDaySteps:[NSDate date]]];
    [self presentPedometerData:[self.pedometerLib getDayStepsFromHealthKitForDevice:[NSDate date] deviceName:[[UIDevice currentDevice] name]]];
}


# pragma mark - Date Methods

- (NSString *)turnDateIntoString:(NSDate *)date {
    
    // create a number formatter
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateStyle = NSDateFormatterShortStyle;
    formatter.timeStyle = NSDateFormatterNoStyle;
    
    return [formatter stringFromDate:date];
}

- (IBAction)queryPedometer:(id)sender {

//    NSMutableDictionary *result = [self.pedometerLib getRangeSteps:self.startDate end:self.endDate];
//    [self presentPedometerDatas:result];

    NSMutableDictionary *hkResult = [self.pedometerLib getRangedStepsFromHealthKitForDevice:self.startDate end:self.endDate deviceName:[[UIDevice currentDevice] name]];
    [self presentPedometerDatas:hkResult];
}

- (IBAction)dateReceived:(UIStoryboardSegue *)segue {
    
    // grab the date from the date picker
    DatePickerViewController *controller = segue.sourceViewController;
    
    // update date properties
    if ([controller.dateType isEqualToString:@"startDate"]) {
        self.startDate = controller.datePicker.date;
    } else {
        self.endDate = controller.datePicker.date;
    }
    
    // update labels
    [self updateLabelWithDate:controller.datePicker.date forType:controller.dateType];
}

- (void)showDatePickerWithType:(NSString *)dateType {
    
    // instantiate our date picker (rather tedious nav controller hierarchy)
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navController = [storyboard instantiateViewControllerWithIdentifier:@"DatePicker"];
    DatePickerViewController *dateController = navController.viewControllers.lastObject;
    dateController.dateType = dateType;

    // give it a date
    if ([dateType isEqualToString:@"startDate"]) {
        dateController.date = self.startDate;
    } else {
        dateController.date = self.endDate;
    }
    
    // and present it
    [self presentViewController:navController animated:YES completion:^{
        
        // do something on completion if necessary
        
    }];
}

- (void)updateLabelWithDate:(NSDate *)date forType:(NSString *)dateType {

    // update start or end date labels
    if ([dateType isEqualToString:@"startDate"]) {
        self.startDateLabel.text = [self turnDateIntoString:date];
    } else {
        self.endDateLabel.text = [self turnDateIntoString:date];
    }
}

- (void)presentPedometerData:(NSNumber *)data {
    
    // make those decimals look handsome
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    formatter.maximumFractionDigits = 2;
    formatter.minimumIntegerDigits = 1;
    
    // setup some strings
    NSString *steps;

    // format our data
    if ([CMPedometer isStepCountingAvailable]) {
        steps = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:data]];
    } else {
        steps = @"不可用";
    }

    NSString *message = [NSString stringWithFormat:@"步数: %@", steps];
    
    // create an alert view
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"结果" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"知道了!" style:UIAlertActionStyleDefault handler:nil];
    [controller addAction:action];
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)presentPedometerDatas:(NSMutableDictionary *)datas {
    // make those decimals look handsome
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc]init];
    formatter.maximumFractionDigits = 2;
    formatter.minimumIntegerDigits = 1;

    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone systemTimeZone]];

    // setup some strings
    NSString *steps;

    NSArray *orderedKeys = [[datas allKeys] sortedArrayUsingSelector:@selector(compare:)];
    NSMutableString *message = [[NSMutableString alloc] init];

    for (NSNumber *dateOffset in orderedKeys) {
        NSNumber *dailyValue = datas[dateOffset];
        steps = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:dailyValue]];

        NSDate *now = [NSDate date];
        NSDate *date = [calendar dateByAddingUnit:NSCalendarUnitDay value:[dateOffset integerValue] toDate:now options:0];

        [message appendFormat:@"日期: %@\t步数: %@\n", [self turnDateIntoString:date], steps];
    }

    // create an alert view
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"结果" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"知道了!" style:UIAlertActionStyleDefault handler:nil];
    [controller addAction:action];

    [self presentViewController:controller animated:YES completion:nil];
}

@end
